﻿using System;
using System.IO;
using System.Text;
using System.Threading;
using RabbitMQ.Client;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Publisher {
    class Program {
        public static void Main(string[] args) {
            var count = 0;
            while (true) {
                Console.Write($"\rConnecting{new string('.', count++ % 4)}".PadRight(80));
                try {
                    var config = JsonConvert.DeserializeObject<JObject>(File.ReadAllText("./config/config.json"));
                    var host = config.Value<string>("host");
                    var exchange = config.Value<string>("exchange");
                    var topic = config.Value<string>("topic");
                    var sleep = config.Value<int>("sleep");

                    var factory = new ConnectionFactory {
                        HostName = host,
                        UserName = "guest",
                        Password = "guest"
                    };

                    using (var connection = factory.CreateConnection())
                    using (var channel = connection.CreateModel()) {
                        var model = connection.CreateModel();

                        channel.ExchangeDeclare(
                            exchange: exchange,
                            type: ExchangeType.Topic,
                            durable: true);

                        while (true) {
                            var message = $"{{\"datetime\":\"{DateTime.Now}\"}}";

                            var properties = model.CreateBasicProperties();

                            channel.BasicPublish(
                                exchange: exchange,
                                routingKey: topic,
                                basicProperties: properties,
                                body: Encoding.UTF8.GetBytes(message));

                            Thread.Sleep(sleep);
                        }
                    }
                } catch (Exception ex)
                  when (ex is RabbitMQ.Client.Exceptions.BrokerUnreachableException ||
                        ex is RabbitMQ.Client.Exceptions.AlreadyClosedException) {
                    Thread.Sleep(1000);
                }
            }
        }
    }
}
