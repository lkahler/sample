﻿using System;
using System.Threading;
using System.Threading.Tasks;
using CSharpFunctionalExtensions;
using Microsoft.Extensions.Logging;

namespace Logger {
    class Program {
        public static void Main() {
            while (true) {
                MainAsync().Result.OnFailure(ex => {
                    Log(LogLevel.Error, ex.Message);
                    Thread.Sleep(1000);
                });
            }
        }

        static void Log(LogLevel logLevel, string message) {
            Console.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss}: {message}");
        }

        static async Task<Result<Success, Exception>> MainAsync() {
            var configResult = Configuration.Create();
            if (configResult.IsFailure) return configResult.Error;
            var config = configResult.Value;

            var loggingService = new LoggingService(Log, config.DatabaseHost);

            var openResult = await loggingService.OpenAsync();
            if (openResult.IsFailure) return openResult.Error;

            using var messageService = new MessageService(Log);
            messageService.Received += (_, message) => loggingService.Log(message);
            loggingService.Logged += (_, message) => messageService.Ack(message);

            openResult = messageService.Open(config.QueueHost);
            if (openResult.IsFailure) return openResult.Error;

            while (true) {
                var processResult = await loggingService.ProcessAsync();
                if (processResult.IsFailure) return processResult.Error;
                await Task.Delay(60000);
            }
        }
    }
}
