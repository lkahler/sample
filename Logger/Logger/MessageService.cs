﻿using System;
using System.Text;
using CSharpFunctionalExtensions;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Client.Exceptions;

namespace Logger {
    public class MessageService : IDisposable {
        public MessageService(Action<LogLevel, string> onLog) {
            _onLog = onLog;
        }

        Action<LogLevel, string> _onLog;
        IConnection? _connection;
        IModel? _model;
        string? _queueName;

        public event EventHandler<Message>? Received;

        public Result<Success, Exception> Open(string host) {
            try {
                var factory = new ConnectionFactory() {
                    HostName = host,
                    UserName = "guest",
                    Password = "guest",
                    AutomaticRecoveryEnabled = true
                };
                _connection = factory.CreateConnection();
                _model = _connection.CreateModel();

                _model.ExchangeDeclare(
                    exchange: "kahler",
                    type: ExchangeType.Topic,
                    durable: true);

                _queueName = _model.QueueDeclare(
                    queue: "firehose",
                    durable: true,
                    exclusive: false,
                    autoDelete: false).QueueName;

                _model.QueueBind(
                    queue: _queueName,
                    exchange: "kahler",
                    routingKey: "#");

                var consumer = new EventingBasicConsumer(_model);
                consumer.Received += (model, args) => {
                    _onLog(LogLevel.Debug, $"Message received {args.DeliveryTag}");

                    var message = new Message(
                        args.DeliveryTag,
                        routingKey: args.RoutingKey,
                        correlationId: args.BasicProperties.CorrelationId,
                        body: Encoding.UTF8.GetString(args.Body.ToArray()));

                    _onLog(LogLevel.Trace, $"{message}");

                    Received?.Invoke(this, message);
                };

                _model.BasicConsume(
                    queue: _queueName,
                    autoAck: false,
                    consumer: consumer);

                return Success.Instance;
            } catch (Exception ex)
               when (ex is BrokerUnreachableException ||
                     ex is AlreadyClosedException) {
                return ex;
            }
        }

        public Result<Success, Exception> Ack(Message message) {
            try {
                if (message is Message m) {
                    _onLog(LogLevel.Debug, $"Ack message {m.DeliveryTag}");
                    _onLog(LogLevel.Trace, $"{m}");
                    _model?.BasicAck(m.DeliveryTag, multiple: false);
                }
                return Success.Instance;
            } catch (Exception ex)
              when (ex is BrokerUnreachableException ||
                    ex is AlreadyClosedException) {
                return ex;
            }
        }

        public void Dispose() {
            _model?.Close();
            _connection?.Close();
        }
    }

    public class Message {
        public Message(
                ulong deliveryTag,
                string? routingKey = null,
                string? correlationId = null,
                string? body = null) {
            DeliveryTag = deliveryTag;
            RoutingKey = routingKey;
            CorrelationId = correlationId;
            Body = body;
        }

        public ulong DeliveryTag { get; private set; }
        public string? RoutingKey { get; private set; }
        public string? CorrelationId { get; private set; }
        public string? Body { get; private set; }

        public override string ToString() {
            var builder = new StringBuilder($"DeliveryTag: {DeliveryTag}");
            if (RoutingKey is string) {
                builder.Append($", RoutingKey: {RoutingKey}");
            }
            if (CorrelationId is string) {
                builder.Append($", CorrelationId: {CorrelationId}");
            }
            if (Body is string) {
                builder.Append($", Body: {Body}");
            }
            return builder.ToString();
        }
    }
}
