﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CSharpFunctionalExtensions;
using Microsoft.Extensions.Logging;

namespace Logger {
    public class LoggingService {
        public LoggingService(Action<LogLevel, string> onLog, string host) {
            _onLog = onLog;
            _host = host;
            _messages = ImmutableList.Create<Message>();
        }

        Action<LogLevel, string> _onLog;
        string _host;
        ImmutableList<Message> _messages;

        public event EventHandler<Message>? Logged;

        public async Task<Result<Success, Exception>> OpenAsync() {
            try {
                if (await DatabaseNotFoundAsync(Utility.Timeout(3))) {
                    await CreateDatabaseAsync(Utility.Timeout(3));
                }

                if (await TableNotFoundAsync(Utility.Timeout(3))) {
                    await CreateTableAsync(Utility.Timeout(3));
                }

                return Success.Instance;
            } catch (Exception ex)
               when (ex is TaskCanceledException ||
                     ex is SqlException) {
                return ex;
            }
        }

        public void Log(Message message) {
            _messages = _messages.Add(message);
        }

        public async Task<Result<Success, Exception>> ProcessAsync() {
            var messages = _messages.ToList();
            _messages = ImmutableList.Create<Message>();
            try {
                await CleanAsync(Utility.Timeout(3));

                while (messages.Any()) {
                    var batch = messages.Take(1000).ToList();
                    var timeout = (int)Math.Max(batch.Count * 0.1, 3);
                    await LogAsync(Utility.Timeout(timeout), batch);
                    batch.ForEach(message => Logged?.Invoke(this, message));
                    messages = messages.Skip(batch.Count).ToList();
                }

                return Success.Instance;
            } catch (Exception ex)
               when (ex is TaskCanceledException ||
                     ex is SqlException) {
                _messages.InsertRange(0, messages);
                return ex;
            }
        }

        SqlConnection Connection => new SqlConnection($"Server={_host};User Id=sa;Password=Kahler6648;");

        async Task<bool> DatabaseNotFoundAsync(CancellationToken cancellationToken) {
            using (var connection = Connection) {
                await connection.OpenAsync(cancellationToken);

                var query = new SqlCommand(
                    $"SELECT COUNT(name) FROM master.sys.databases WHERE name='messages'",
                    connection);

                using (var reader = await query.ExecuteReaderAsync(cancellationToken)) {
                    var count = await reader.ReadAsync(cancellationToken) ? (int)reader[0] : 0;
                    return count == 0;
                }
            }
        }

        async Task CreateDatabaseAsync(CancellationToken cancellationToken) {
            using (var connection = Connection) {
                await connection.OpenAsync(cancellationToken);

                _onLog(LogLevel.Information, "Create messages database");

                await new SqlCommand($"CREATE DATABASE [messages]", connection).ExecuteNonQueryAsync(cancellationToken);
            }
        }

        async Task<bool> TableNotFoundAsync(CancellationToken cancellationToken) {
            using (var connection = Connection) {
                await connection.OpenAsync(cancellationToken);

                var query = new SqlCommand(
                    "SELECT COUNT(TABLE_NAME) " +
                    "FROM messages.INFORMATION_SCHEMA.TABLES " +
                    "WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='messages'",
                    connection);

                using (var reader = await query.ExecuteReaderAsync(cancellationToken)) {
                    var count = await reader.ReadAsync(cancellationToken) ? (int)reader[0] : 0;
                    return count == 0;
                }
            }
        }

        async Task CreateTableAsync(CancellationToken cancellationToken) {
            using (var connection = Connection) {
                await connection.OpenAsync(cancellationToken);

                _onLog(LogLevel.Information, "Create messages table");

                await new SqlCommand(
                    "CREATE TABLE [messages].dbo.[messages] (" +
                    "[deliveryTag] BINARY(8) NOT NULL," +
                    "[logged] DATETIME NOT NULL," +
                    "[routingKey] VARCHAR(255)," +
                    "[correlationId] VARCHAR(36)," +
                    "[body] VARCHAR(MAX))",
                    connection).ExecuteNonQueryAsync(cancellationToken);
            }
        }

        async Task LogAsync(CancellationToken cancellationToken, IEnumerable<Message> messages) {
            int count = messages.Count();

            _onLog(LogLevel.Debug, $"Write {count} messages to logs table");

            using (var connection = Connection) {
                await connection.OpenAsync(cancellationToken);

                await new SqlCommand(
                    "INSERT INTO [messages].dbo.[messages] " +
                    "([deliveryTag],[logged],[routingKey],[correlationId],[body]) VALUES " +
                    string.Join(",", messages.Select(ToSql)),
                    connection).ExecuteNonQueryAsync(cancellationToken);
            }
        }

        Func<Message, string> ToSql = (Message message) => {
            var values = new string[] {
                $"0x{message.DeliveryTag:X}",
                "GETDATE()",
                $"'{message.RoutingKey?.Clip(255).Escape() ?? "NULL"}'",
                $"'{message.CorrelationId?.Clip(36).Escape() ?? "NULL"}'",
                $"'{message.Body?.Escape() ?? "NULL"}'"
            };
            return $"({string.Join(",", values)})";
        };

        async Task CleanAsync(CancellationToken cancellationToken) {
            using (var connection = Connection) {
                await connection.OpenAsync(cancellationToken);

                _onLog(LogLevel.Debug, "Clean messages table");

                await new SqlCommand(
                    $"DELETE FROM [messages].dbo.[messages] WHERE DATEDIFF(day, [logged], GETDATE()) > 7",
                    connection).ExecuteNonQueryAsync(cancellationToken);
            }
        }
    }

    static class LoggingServiceExtensions {
        public static string Escape(this string text) => text.Replace("'", "''");

        public static string Clip(this string text, int maxLength) {
            if (text.Length > maxLength) text = text.Substring(0, maxLength);
            return text;
        }
    }
}
