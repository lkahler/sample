﻿using System;
using System.IO;
using CSharpFunctionalExtensions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;


namespace Logger {
    public class Configuration {
        public static Result<Configuration, Exception> Create() {
            try {
                return new Configuration(
                    JsonConvert.DeserializeObject<JObject>(File.ReadAllText("./config/config.json")));
            } catch (IOException ex) {
                return ex;
            } catch (JsonSerializationException ex) {
                return ex;
            }
        }

        Configuration(JObject config) {
            QueueHost = config.Value<string>("queueHost");
            DatabaseHost = config.Value<string>("dbHost");
        }

        public string QueueHost { get; private set; }
        public string DatabaseHost { get; private set; }
    }
}
