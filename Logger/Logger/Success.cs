﻿using System;

namespace Logger {
    public class Success {
        Success() { }

        static Success _instance = new Success();
        static public Success Instance => _instance;
    }
}
