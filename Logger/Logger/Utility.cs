﻿using System.Threading;

namespace Logger {
    public class Utility {
        public static CancellationToken Timeout(int seconds) => new CancellationTokenSource(seconds * 1000).Token;
    }
}
