﻿using System;
using System.IO;
using System.Text;
using System.Threading;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Subscriber {
    class Program {
        public static void Main() {
            while (true) {
                try {
                    var config = JsonConvert.DeserializeObject<JObject>(File.ReadAllText("./config/config.json"));
                    var host = config.Value<string>("host");
                    var exchange = config.Value<string>("exchange");
                    var topic = config.Value<string>("topic");
                    var queue = config.Value<string>("queue");
                    var durable = false;
                    var autoDelete = true;

                    var factory = new ConnectionFactory() { HostName = host, UserName = "guest", Password = "guest" };
                    using (var connection = factory.CreateConnection())
                    using (var channel = connection.CreateModel()) {
                        channel.ExchangeDeclare(
                            exchange: exchange,
                            type: ExchangeType.Topic,
                            durable: true);

                        var queueName = channel.QueueDeclare(
                            queue: queue,
                            durable: durable,
                            exclusive: false,
                            autoDelete: autoDelete).QueueName;

                        channel.QueueBind(
                            queue: queueName,
                            exchange: exchange,
                            routingKey: topic);

                        var consumer = new EventingBasicConsumer(channel);

                        consumer.Received += (model, args) => {
                            var message = Encoding.UTF8.GetString(args.Body.ToArray());
                            Console.WriteLine($"Received: {message}");
                        };

                        channel.BasicConsume(
                            queue: queueName,
                            autoAck: true,
                            consumer: consumer);

                        while (true) Thread.Sleep(1000);
                    }
                } catch (Exception ex)
                  when (ex is RabbitMQ.Client.Exceptions.BrokerUnreachableException ||
                        ex is RabbitMQ.Client.Exceptions.AlreadyClosedException) {
                    Thread.Sleep(1000);
                }
            }
        }
    }
}
